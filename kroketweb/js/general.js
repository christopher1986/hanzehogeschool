(function(window, document, undefined) {
    
	/**
	 * Returns the closest element that matches with the given selector, or null if no 
	 * matching element was found.
	 *
	 * @param string selector a selector to find the closest element.
	 * @return object|null an HTMLElement object for the matching element, or null
	 *					   if no element was found.
	 */
	function closest(selector) {		
		if (typeof selector === 'string') {
			// remove white-space from both sides.
			selector = selector.trim();
			// the current DOM element.
			var el = this;
			while (el && el.parentNode) {
				el = el.parentNode;
				
				// determine if the element matches with selector.
				var matched = false;
				if (el.id && selector.startsWith('#')) {
					matched = (el.id == selector.substr(1));
				} else if (el.className && selector.startsWith('.')) {
					// array containing class names from this element.
					var classNames = el.className.split(' ');
					
					// remove first period from the selector.
					var classSelector = selector.substr(1);
					if (classSelector.contains('.')) {
						// array of class names that must match.
						var classSelectors = classSelector.split('.');
						
						// only true if the retained array is same size as array with classes to match.
						matched = (classNames.retainAll(classSelectors).length == classSelectors.length);
					} else {
						// true if class selector is contained by this element.
						matched = (classNames.indexOf(classSelector) > -1);
					}
				} else if (el.tagName) {
					// array of possible tag names to match.
					var tagNames = selector.split(' ');
					// true if the array contains tag name of this element.
					matched = (tagNames.indexOf(el.tagName.toLowerCase()) > -1);
				}
				
				// return matched element.
				if (matched) {
					return el;
				}
			}
		} else if (this.parentNode) {
			// return first parent.
			return this.parentNode;
		}
	}
	
	/**
	 * Returns a NodeList object if the given selector consists of class names or if the selector consists of one or more tag names. 
	 * A single Element object may be returned if the selector starts with a hashtag (#), which means an element is searched for 
	 * by it's id.
	 *
	 * @param string selector the selector to retrieve DOM elements for.
	 * @return object|null an Element object if the selector is targeted for a single element by id, or a NodeList object
	 *					   if the selector is targeted for class or tag names.
	 */
	function filter(selector) {
		// first argument should be a string.
		if (typeof selector !== 'string') {
			return;
		}
		
		// remove white-space from both sides.
		selector = selector.trim();
		if (selector.startsWith('#')) {
			return this.getElementById(selector.substr(1));
		} else if (selector.startsWith('.')) {
			/*
			 * We prefer getElementsByClassName() since it's fastest way to find elements by their 
			 * class names, but Internet Explorer 8 and lower have no support for it so we use
			 * querySelectorAll() as a fall back function.
			 *
			 * @link http://jsperf.com/getelementsbyclassname-vs-queryselectorall/18
			 */
			if (document.getElementsByClassName) {									
                // hold one or more class names.
				var classNames = [];
				
				// remove first period from selector.
				var classSelector = selector.substr(1);
				if (classSelector.contains('.')) {								
					// get multiple class names.
					var classSelectors = classSelector.split('.');
					for (var index in classSelectors) {
						var className = classSelectors[index];
						if (className.startsWith('.')) {
							classNames.push(className.substr(1));
						}
					}
				} else {
					classNames.push(classSelector);
				}

				// find elements by joining class names into a white-space separated string.
				return this.getElementsByClassName(classNames.join(' '));
			} else {
				// find elements with the original selector.
				return this.querySelectorAll(selector);
			}
		} else {
			if (selector.hasWhiteSpace()) {
				// get multiple tag names.
				var tags = selector.split(' ');
				// find elements by multiple tag names.
				return this.querySelectorAll(tags.join(','));
			} else {
				// find elements by their tag name.
				return this.querySelectorAll(selector);
			}
		}
	}
	
	// only add function to prototype is it doesn't exist.
	if (typeof HTMLDocument.prototype.filter !== 'function') {
		/**
		 * Returns element(s) that match the given selector.
		 * 
		 * @param string selector the selector to retrieve element for.
		 * @return object|null returns the element(s) that matched the 
		 *					  given selector, or null.
		 * @see filter(selector);
		 */
		HTMLDocument.prototype.filter = filter;
	}
	
	// only add function to prototype is it doesn't exist.
	if (typeof HTMLElement.prototype.filter !== 'function') {
		/**
		 * Returns element(s) that match the given selector.
		 * 
		 * @param string selector the selector to retrieve element for.
		 * @return objec|null the element(s) that matched the given selector,
		 *					  or null.
		 * @see filter(selector);
		 */
		HTMLElement.prototype.filter = filter;
	}
	
	// only add function to prototype is it doesn't exist.
	if (typeof HTMLElement.prototype.closest !== 'function') {
		/**
		 * Returns the first closest element that matched the given selector.
		 * 
		 * @param string selector a selector to retrieve the closest element.
		 * @return objec|null an element that matched the given selector, or null.
		 * @see closest(selector);
		 */
		HTMLElement.prototype.closest = closest;
	}
	
	if (typeof HTMLElement.prototype.hassClass !== 'function') {
	    /**
	     * Returns true if this HTMLElement contains the specified class name, false otherwise.
	     *
	     * @param string className the class name whose presence will be tested.
	     * @return bool true if the given class name is contained by this HTMLElement.
	     */
	    HTMLElement.prototype.hasClass = function(className) {
	        // create array containing class names
	        var classNames = this.className.split(' ');
	        if (typeof className === 'string') {
	            return classNames.contains(className);
            }
            return false;
	    }
	}
	
	if (typeof HTMLElement.prototype.addClass !== 'function') {
	    /**
	     * Add one or more classes to the selected HTMLElement.
	     *
	     * @param string One or more class names to add.
	     */
	    HTMLElement.prototype.addClass = function(classNames) {
	        if (typeof classNames === 'string') {
                // create array containing class names to add.
                classNames = classNames.split(' ');
                // create array containing new class names.
                newClassNames = this.className.split(' ').removeAll(classNames).concat(classNames);
                // replace old class names with new ones.
                this.className = newClassNames.join(' ').trim();
	        }
	    }
	}
	
	if (typeof HTMLElement.prototype.removeClass !== 'function') {
	    /**
	     * Remove one or more classes to the selected HTMLElement.
	     *
	     * @param string One or more class names to remove.
	     */
	    HTMLElement.prototype.removeClass = function(classNames) {
	        if (typeof classNames === 'string') {
	            // create array containing class names to remove.
	            classNames = classNames.split(' ');
	            // create array containing valid class names.
	            var newClassNames = this.classNames.split(' ').removeAll(classNames);
                // replace old class names with new ones.
	            this.classNames = newClassNames.join(' ').trim();
            } 
	    }
	}
	
	// only add function to prototype if it doesn't exist.
	if (typeof Array.prototype.indexOf !== 'function') {
		/**
		 * Returns the index of the first occurrence that matches with the given needle,
		 * or -1 if the array does not contain the given needle.
		 *
		 * @param mixed needle the value to search for.
		 * @return int the index of the first occurrence, or -1 if not found.
		 */
		Array.prototype.indexOf = function(needle) {
			for(var i = 0; i < this.length; i++) {
				if(this[i] === needle) {
					return i;
				}
			}
			return -1;
		};
	}
	
	// only add function to prototype if it doesn't exist.
	if (typeof Array.prototype.retainAll !== 'function') {
		/**
		 * Returns a new array whose values are contained in the specified array.
		 * In other words remove all values from this array that are not present in the 
		 * specified array.
		 *
		 * @param array arr1 an array to compare with.
		 * @return array an array with retained values.
		 */
		Array.prototype.retainAll = function(arr1) {
			var retval = [];
			if (Object.prototype.toString.call(arr1) === '[object Array]') {
				for (var i = 0; i < arr1.length; i++) {
					if (this.indexOf(arr1[i]) > -1) {
						retval.push(arr1[i]);
					}
				}
			}
			return retval;
	    }
	}
	
	if (typeof Array.prototype.removeAll !== 'function') {
	    /**
	     * Removes a new array whose values are not contained in the specified array.
	     * In ohter words retain values from this array that are not present in the
	     * specified array.
	     *
	     * @param array arr1 an array containing values to remove.
	     * @return array an array containing values that were not removed.
	     */
		Array.prototype.removeAll = function(arr1) {
			var retval = [];
			if (Object.prototype.toString.call(arr1) === '[object Array]') {
				for (var i = 0; i < this.length; i++) {
					if (arr1.indexOf(this[i]) == -1) {
			            retval.push(this[i]);
					}
				}
			}
			return retval;
	    }
	}
	
	if (typeof Array.prototype.contains !== 'function') {
	    /**
	     * Returns true if this array contains the specified element, false otherwise.
	     *
	     * @param mixed obj element whose presence in this array will be tested.
	     * @return bool true if this array contains the specified element.
	     */
	    Array.prototype.contains = function (obj) {
            for (var i = 0; i < this.length; i++) {
                if (this[i] === obj) {
                    return true;
                }
            }
            return false;
	    }
	}
	
	
	
	// only add function to prototype if it doesn't exist.
	if (typeof String.prototype.format !== 'function') {
		/**
		 * Returns a formatted string with the given number of arguments.
		 *
		 * @param mixed arguments a variable number of arguments.
		 * @return string a formatted string.
		 */
		String.prototype.format = function() {
			var args = arguments;
			return this.replace(/{(\d+)}/g, function(match, number) { 
				return (typeof args[number] != 'undefined') ? args[number] : match;
			});
		};
	}
	
	// only add function to prototype if it doesn't exist.
	if (typeof String.prototype.contains !== 'function') {
		/**
		 * Returns true if and only if this string contains the specified sequence of char values.
		 *
		 * @param string s the sequence to search for.
		 * @return bool true if this string contains s, false otherwise.
		 */
		String.prototype.contains = function(s){
			return (this.indexOf(s) !== -1);
		};
	}
	
	// only add function to prototype if it doesn't exist.
	if (typeof String.prototype.startsWith !== 'function') {
		/**
		 * Test if this string starts with the specified prefix.
		 *
		 * @param string prefix the prefix.
		 * @return bool true if the character sequence represented by the argument if a prefix
		 *				of the character sequence represented by this string, false otherwise.
		 */
		String.prototype.startsWith = function(prefix){
			return (this.indexOf(prefix) == 0);
		};
	}
	
	// only add function to prototype is it doesn't exist.
	if (typeof String.prototype.hasWhiteSpace !== 'function') {
		/**
		 * Test if this string has any white-space.
		 *
		 * Use the trim() function first if you only wish to test for white-space
		 * between the first and last character.
		 *
		 * @return bool true if the character sequence represented by this string has any
		 *				white-space, false otherwise.
		 */
		String.prototype.hasWhiteSpace = function() {
			return /\s/g.test(this);
		}
	}
	
	// only add function to prototype is it doesn't exist.
	if (typeof String.prototype.trim !== 'function') {
		/**
		 * Removes white-space from both sides of the string.
		 *
		 * @return a string with white-space removed from both sides.
		 */
		function trim() {
			return this.replace(/^\s+|\s+$/gm,'');
		}
	}
	
	// only add function to prototype is it doesn't exist.
	if (typeof String.prototype.ltrim !== 'function') {
		/**
		 * Removes white-space from the beginning of the string.
		 *
		 * @return a string with white-space removed from the beginning.
		 */
		String.prototype.ltrim = function() {
			return this.replace(/^\s+/,"");
		}
	}

	// only add function to prototype is it doesn't exist.
	if (typeof String.prototype.rtrim !== 'function') {
		/**
		 * Removes white-space from the beginning of the string.
		 *
		 * @return a string with white-space removed from the beginning.
		 */
		String.prototype.rtrim = function() {
			return this.replace(/\s+$/,"");
		}
	}
    
    // only add function to prototype if it doesn't exist.
    if (typeof Number.prototype.formatMoney !== 'function') {
        /**
         * Returns a formatted currency string from a number.
         *
         * @param int places the amount of number to place behind the decimal separator.
         * @param string symbol the symbol to place before the currency.
         * @param thousand the character to use as thousand separator.
         * @param decimal the character to use as decimal separator.
         * @return string a string formatted as currency.
         * @link http://www.josscrowcroft.com/2011/code/format-unformat-money-currency-javascript/
         */
        Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
            places = (!isNaN(places = Math.abs(places))) ? places : 2;
            symbol = (symbol !== undefined) ? symbol : "$";
            thousand = thousand || ",";
            decimal = decimal || ".";
            var number = this, 
                negative = number < 0 ? "-" : "",
                i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
                j = (j = i.length) > 3 ? j % 3 : 0;
            return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
        };
    }
        
})(this, this.document); // Pass in a reference to the original global window object.
