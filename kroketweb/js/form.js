(function(window, document, undefined) {
    /**
     * A Form object is capable of validating form fields based on a set of rules. 
     * New rules can be added through the use of privileged methods, these rules 
     * however are only applicable to the object they are added to and will need 
     * to be added again when a new Form object is created.
     *
     * @author Chris Harris
     * @version 1.0.0
     */ 
	function Form(args) {        
        /**
         * An object containing rules used to validate form fields.
         *
         * @var object
         */
        var rules = {
            regex: function(element, args) {
                var settings = extend({
                    pattern: '.*',
                    flags : '',
                    message: 'Dit veld is verplicht.'
                }, args);
                
                if (element instanceof HTMLInputElement) {
                    var re = new RegExp(settings.pattern, settings.flags);
                    if (re.test(element.value)) {
                        return true;
                    }
                }
                return settings.message;
            },
            required: function(element, args) {
                var settings = extend({
                    message: 'Dit veld is verplicht.'
                }, args);
                
                if (element instanceof HTMLInputElement) {
                    switch(element.getAttribute('type')) {
                        case 'radio':
                            // rule matched for radio buttons.
                            for (var i = 0; i < element.length; i++) {
                                if (element[0].checked) {
                                    return true;
                                }
                            }
                            break;
                        case 'checkbox':
                            // rule matched for checkbox.
                            if (element.checked) {
                                return true;
                            }
                            break;
                        case 'text':
                        case 'password':
                            // rule matched for text fields.
                            if (element.value.length) {
                                return true;
                            }
                            break;
                    };
                } else if ((element instanceof HTMLInputElement) && element.value.length) {
                    return true;
                }
                
                return settings.message;
            },
            phone: function(element, args) {            
                // validate phone number using a regular expression.
                return rules.regex(element, extend({
                    pattern: '^\\\+?[\\d\\s]+$',
                    message: 'Dit is geen geldig telefoonnummer.'
                }, args));
            }
        };
    
        /**
         * Settings used by this object for form validation.
         *
         * @var object
         */
        var settings = {
            validateAttribute: 'data-validate'
        };
    
        /**
         * Merge the contents of two or more objects together into the first object.
         *
         * @param deep a boolean value indicating if the objects should be merged recursively (optional).
         * @param target the object to extend.
         * @param object1 an object containing additional properties to merge in.
         * @param objectN additional objects containing properties to merge in.
         * @return object the modified target object containing merged properties, to preserve both
         *                of the original objects you can pass an empty object as the target.
         */
        function extend() {
            // convert arguments object to a real Array.
            var args = Array.prototype.slice.call(arguments);
        
            // determine if to deep copy objects.
            var deep = false;
            if (args.length > 2 && Object.prototype.toString.call(args[0]) !== '[object Object]') {
                // get and remove first argument.
                deep = Boolean(args.splice(0, 1));
            }

            var retval = {};
            if (args.length) {        
                var i, j = 0;
                for (i = (args.length - 1); i >= 1; i--) {
                    j = (i - 1);
                    var prevObj = args[j];
                    if (Object.prototype.toString.call(prevObj) !== '[object Object]') {
                        prevObj = {};
                    }
                    
                    var currentObj = args[i];
                    if (Object.prototype.toString.call(prevObj) === '[object Object]') {
                        for (prop in currentObj) {
                            // skip the inherited properties from prototype.
                            if(currentObj.hasOwnProperty(prop)) {
                                if (deep && prevObj.hasOwnProperty(prop)
                                        && Object.prototype.toString.call(currentObj[prop]) === '[object Object]' 
                                        && Object.prototype.toString.call(prevObj[prop]) === '[object Object]') {
                                    prevObj[prop] = extend(deep, prevObj[prop], currentObj[prop]);
                                } else {
                                    prevObj[prop] = currentObj[prop];
                                }
                            }
                        }
                    }
                }
                retval = args[j];
            }
            
            return retval;
        }
        
        /**
         * Returns an object containing extracted arguments from the given string.
         *
         * The following rule 'regex(pattern="abc", message="not a valid value.")'
         * will return an object with the following keys 'pattern', 'message' which
         * have the following values 'abc' and 'not a valid value.' respectively.
         *
         * @param string rule a string containing a validation rule.
         * @return object an object containing all arguments extracted from the rule.
         */
        function getRuleArgs(rule){
            var ruleArgs = {};
        
            var re = new RegExp('\\((.*)\\)');
            if (matches = re.exec(rule)) {
                // retrieve all arguments from rule string.
                var args = matches[1].split(',');
                for (var index = 0; index < args.length; index++) {
                    // retrieve key-value pair from argument string.
                    var keyValuePair = args[index].trim().split('=');
                    if (keyValuePair.length >= 2) {
                        // add a new rule argument from key-value pair.
                        ruleArgs[keyValuePair[0]] = keyValuePair[1];
                    }
                }
            }
            return ruleArgs;
        }
    
        /**
         * Add a new validation rule.
         *
         * @param string name a name to identify this validation rule.
         * @param object a callable function to call containing validation logic.
         */
        this.addRule = function(name, callable) {
            if (typeof callable === 'function') {
                rules[name] = callable;
            }
        }
        
        /**
         * Remove an existing validation rule.
         *
         * @param string name the name of an existing validation rule.
         */
        this.removeRule = function(name) {
            if (hasRule(name)) {
                delete rules[name];
            }
        }
        
        /**
         * Returns true if a validation rule exists with the given name, false otherwise.
         *
         * @param string name the name of a rule whose existence will be tested.
         * @return bool true if a validation rule was found, false otherwise.
         */
        this.hasRule = function(name) {
            return rules.hasOwnProperty(name);
        }
    
        /**
         * A handler to start validating all form fields.
         *
         * @param FocusEvent e an focus event object.
         */
        function validationHandler(e) {
            var form = e.srcElement;
            if (typeof form !== 'undefined') {
                // remove field errors from previous validations.
                var errorElements = form.filter('.field-error');
                for (i = 0; i < errorElements.length; i++) {
                    errorElements[i].remove();
                }
            
                // validate all form fields with a specific data attribute.
                var results = form.filter('*[{0}]'.format(settings.validateAttribute));
                if (results.length) {
                    for (i = 0; i < results.length; i++) {                    
                        // HTML element containing a rule attribute.
                        var element = results[i];
                        // get contents of rule attribute. 
                        var attr = element.getAttribute(settings.validateAttribute);             
                        
                        // search for a rule name in this attribute.
                        var re = new RegExp('.[^(]*');
                        var ruleName = re.exec(attr);
                        
                        if (rules.hasOwnProperty(ruleName)) {
                            // the name of the rule to call.
                            var rule = rules[ruleName];
                            if (typeof rule === 'function') {
                                // invoke callable function and store the result.
                                var ruleResult = rule.call(this, element, getRuleArgs(attr));
                                if ((typeof ruleResult === 'string')) {
                                    // find existing error element(s).
                                    var errorElements = element.parentNode.filter('.field-error');
                                    if (errorElements.length) {
                                        // empty this error element.
                                        var errorElement = errorElements[0];
                                        while (errorElement.firstChild) {
                                            errorElement.removeChild(errorElement.firstChild);
                                        }
                                        
                                        // repopulate element with error message.
                                        errorElement.appendChild(document.createTextNode(ruleResult));
                                    } else {
                                        // create a new element to hold the error message.
                                        var errorElement = document.createElement('div');
                                        errorElement.appendChild(document.createTextNode(ruleResult));
                                        errorElement.addClass('field-error');
                                        
                                        // insert new element after the form field.
                                        element.parentNode.insertBefore(errorElement, element.nextSibling);
                                    }
                                } else {
                                    // find existing error element(s).
                                    var errorElements = element.parentNode.filter('.field-error');
                                    if (errorElements.length) {
                                        // remove error element(s).
                                        for (var index = 0; index < errorElements.length; index++) {
                                            errorElements[index].parentNode.removeChild(errorElements[index]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // prevent form submission.
            e.preventDefault();
        }
    
	    /**
	     * Initializes variables, listeners and populates the necessary DOM elements 
	     * with data.
         *
         * @param object args optional argument to change form validation.
	     */
        function init(args) {
            // merge the given arguments with default settings.
            if (Object.prototype.toString.call(args) === '[object Object]') {
                settings = extend(settings, args);
            }
        
            // HTMLCollection with elements that match the CSS selector.
            var results = document.filter('.order-form');
            if (results.length) {
                results[0].addEventListener('submit', validationHandler);
            }
        }
        init(args); 
    }
    
	// add Cart object to the global scope.
	window.Form = Form;
    
})(this, this.document); // Pass in a reference to the original global window object.