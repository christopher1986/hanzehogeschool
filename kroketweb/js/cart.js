(function(window, document, undefined) {
    /**
     * A Cart object stores and retrieves items from a local storage.
     * Items can be added or removed from the cart through the use of 
     * privileged methods.
     *
     * @author Chris Harris
     * @version 1.0.0
     */
	function Cart() {
    
        /**
         * An HTMLTableElement in which products are stored.
         *
         * @var HTMLTableElement
         */
        var cart;
    
		/**
		 * Test if the given argument is a numeric value.
		 *
		 * @param mixed value the value to test.
		 * @return bool true if the given argument is considered to be a numeric value,
		 *				false otherwise.
		 */
		function isNumeric(value) {
			return (!isNaN(parseFloat(value)) && isFinite(value));
		}
	
        /**
         * Merge the contents of two or more objects together into the first object.
         *
         * @param deep a boolean value indicating if the objects should be merged recursively (optional).
         * @param target the object to extend.
         * @param object1 an object containing additional properties to merge in.
         * @param objectN additional objects containing properties to merge in.
         * @return object the modified target object containing merged properties, to preserve both
         *                of the original objects you can pass an empty object as the target.
         */
        function extend() {
            // convert arguments object to a real Array.
            var args = Array.prototype.slice.call(arguments);
        
            // determine if to deep copy objects.
            var deep = false;
            if (args.length > 2 && Object.prototype.toString.call(args[0]) !== '[object Object]') {
                // get and remove first argument.
                deep = Boolean(args.splice(0, 1));
            }

            var retval = {};
            if (args.length) {        
                var i, j = 0;
                for (i = (args.length - 1); i >= 1; i--) {
                    j = (i - 1);
                    var prevObj = args[j];
                    if (Object.prototype.toString.call(prevObj) !== '[object Object]') {
                        prevObj = {};
                    }
                    
                    var currentObj = args[i];
                    if (Object.prototype.toString.call(prevObj) === '[object Object]') {
                        for (prop in currentObj) {
                            // skip the inherited properties from prototype.
                            if(currentObj.hasOwnProperty(prop)) {
                                if (deep && prevObj.hasOwnProperty(prop)
                                        && Object.prototype.toString.call(currentObj[prop]) === '[object Object]' 
                                        && Object.prototype.toString.call(prevObj[prop]) === '[object Object]') {
                                    prevObj[prop] = extend(deep, prevObj[prop], currentObj[prop]);
                                } else {
                                    prevObj[prop] = currentObj[prop];
                                }
                            }
                        }
                    }
                }
                retval = args[j];
            }
            
            return retval;
        }
		
		/**
		 * Returns a normalized string.
		 *
		 * The given string will have it's hyphens and white-space replaced by an underscore.
		 * Any non alphanumeric character will be removed from the string.
		 *
		 * @param string name the string to normalize.
		 * @return string a normalized string.
		 */
		function normalize(name) {
			// cast to String object if necessary.
			if (typeof name !== 'string') {
				name = String(name);
			}
			
			// replace invalid characters to create a normalized string.
			return name.replace(/[-\s]/g, '_').replace(/[^_\w]/gi, '').toLowerCase();
		}

        /**
         * A handler for an anchor element to remove a spefic item from the cart.
         *
         * @param FocusEvent e an focus event object.
         */
        function removeItenHandler(e) {
            if (e.target.hasAttribute('data-item-id')) {
                // get storage key from dat attribute.
                var storageKey = e.target.getAttribute('data-item-id');
                if (storageKey) {
                    // remove item associated with key.
                    sessionStorage.removeItem(storageKey);
                }
            }
            
		    // clear items from cart.
		    clearCart();
		    // repopulate cart with items.
		    populateCart();
        }

		/**
		 * Loads products from the session storage. After this function returns the cart will be
		 * populated with products.
		 */
		function populateCart() {
			// get HTMLTableElement containing products.
			var cart = getCart();
			if (cart instanceof HTMLTableElement) {
				var grandTotalPrice = 0;
				for (i = 0; i < sessionStorage.length; i++) {
					// retrieve storage key.
					var storageKey = sessionStorage.key(i);
					if (typeof sessionStorage.getItem(storageKey) === 'string') {
						// parse JSON string to object.
						var product = JSON.parse(sessionStorage.getItem(storageKey));
						// calculate the total price for this product.
						var totalPrice = product.quantity * product.price;
						// delete button to remove this item from the storage.
						var deleteButton = document.createElement('a');
						deleteButton.appendChild(document.createTextNode("verwijder"));
						deleteButton.addClass('delete-cart-item');
						deleteButton.setAttribute('data-item-id', storageKey);
						deleteButton.addEventListener('click', removeItenHandler);
						
						// insert new row to hold product data.
						var row = cart.insertRow(-1);
						// the product name cell.
						var firstCell = row.insertCell(-1);
						firstCell.appendChild(document.createTextNode(product.name));
						// the quantity cell.
						var secondCell = row.insertCell(-1);
						secondCell.appendChild(document.createTextNode(product.quantity));
						// the price cell.
						var thirdCell = row.insertCell(-1);
						thirdCell.appendChild(document.createTextNode(product.price.formatMoney(2, '€', '.', ',')));
						// the total price cell.
						var fourthCell = row.insertCell(-1);
						fourthCell.appendChild(document.createTextNode(totalPrice.formatMoney(2, '€', '.', ',')));
						// delete item button.
						var fifthCell = row.insertCell(-1);
						fifthCell.appendChild(deleteButton);
						
						grandTotalPrice += totalPrice;
					}
				}
				
				// insert a final row.
				var row = cart.insertRow(-1);
				row.className = 'grand-total-row';
				// the grand total name cell.
				var firstCell = row.insertCell(-1);
				firstCell.appendChild(document.createElement('div').appendChild(document.createTextNode('Totaal:')));
				firstCell.colSpan = 3;
				// the grand total price cell.
				var secondCell = row.insertCell(-1);
				secondCell.colSpan = 2;
				secondCell.appendChild(document.createTextNode(grandTotalPrice.formatMoney(2, '€', '.', ',')));
			}
		}
		
		/**
		 * Remove all products from cart. After this function returns the cart will be empty.
		 */
		function clearCart() {
			// get HTMLTableElement containing products.
			var cart = getCart();
			if (cart instanceof HTMLTableElement) {
				var results = cart.filter('tbody');
				if (results.length) {
					// replace old tbody with new one.
					cart.replaceChild(results[0], document.createElement('tbody'));
				} else {
					// delete all rows except head row.
					for (i = (cart.rows.length - 1); i > 0; i--) {
						cart.deleteRow(i);
					}
				}
			}
		}
	
		/**
		 * Add a new item to the shopping cart.
		 * 
		 * @param object item the item to add to the cart.
		 */
		this.addItem = function(args, id) {
			var product = extend(true, {}, {
				name: '',
				quantity: 0,
				price: 0
			}, args);
			
			// remove non-numeric characters (except comma/minus sign).
			if (typeof product.price === 'string') {
				product.price = parseFloat(product.price.replace(/[,]/, '.').replace(/[^0-9-.]/g, ''));
			}
			
			// make sure the browser supports storage.
			if (typeof(Storage) !== "undefined") {
				// a unique key for this product.
				var storageKey = normalize(product.name);
				// save product to storage.
				sessionStorage.setItem(storageKey, JSON.stringify(product));
			}
			
			// remove all items from the cart.
			clearCart();
			// repopulate cart with items.
			populateCart();
		}
		
		/**
		 * Remove all items from the shopping cart.
		 */
		this.clear = function() {
			clearCart();
		}
		
		/**
		 * Remove item with the given index from the shopping cart.
		 *
		 * @param int index the index of the row to delete.
		 */
		this.removeItem = function(index) {
			// get shopping cart.
			var cart = getCart();
			if (cart instanceof HTMLTableElement) {
				if (isNumeric(index) && (index < cart.rows.length)) {
					cart.deleteRow(index);
				}
			}
		}
		
		/**
		 * Returns an HTMLTableElement that represents our shopping cart, or null
	     * if the DOM document contains no shopping cart.
		 *
		 * @return HTMLTableElement|null an HTML element, or null if no shopping
		 *							cart was found.
		 */
		function getCart() {
			if (typeof cart === 'undefined') {
				var results = document.filter('table#shopping-cart');
				if (results instanceof NodeList) {
					if (results.length) { 
						cart = results[0];
					}
				} else if (results instanceof HTMLElement) {
					cart = results;
				}
			}
			return cart;
		}
		
		// clear items from cart.
		clearCart();
		// repopulate cart with items.
		populateCart();
	}
	
	// add Cart object to the global scope.
	window.Cart = Cart;
	
})(this, this.document); // Pass in a reference to the original global window object.
