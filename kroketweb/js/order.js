(function(window, document, undefined) {
	
	/**
	 * Add or remove items from the cart.
	 *
	 * @var Cart
	 */
	var cart;
	
	/**
	 * Test if the given argument is a numeric value.
	 *
	 * @param mixed value the value to test.
	 * @return bool true if the given argument is considered to be a numeric value,
	 *				false otherwise.
	 */
	function isNumeric(value) {
		return (!isNaN(parseFloat(value)) && isFinite(value));
	}
	
	/**
	 * A handler for input fields that stores the order quantity
	 * for each product.
	 *
	 * @param FocusEvent e an focus event object.
	 */
	function orderAmountHandler(e) {
		if (this.hasOwnProperty('value')) {
			var quantity = this.value;
			if (isNumeric(quantity)) {
				// round quantity downward to a whole number. 
				this.value = quantity = Math.floor(quantity);
				
				// get the closest table row.
				var row = this.closest('tr');
				if (row) {
					// get all cells for this row.
					var cells = row.filter('td');
					if (cells) {
						cart.addItem({
							name: cells[0].innerText,
							quantity: quantity,
							price: cells[1].innerText
						}, row.rowIndex);
					}
				}
			} else {
				// clear invalid values.
				this.value = null;
			}
		}
	}
	
	/**
	 * Initializes variables, listeners and populates the necessary DOM elements 
	 * with data.
	 */
	function init() {
		// lazy initialize our Cart object.
		if (typeof cart === 'undefined' || !(cart instanceof Cart)) {
			cart = new Cart();
		}
	
		// find the assortment element first.
		var assortment = document.filter('#assortment');
		if (assortment) {
			// find input fields inside the assortment element.
			var inputs = assortment.filter('input[type=text]');
			if (inputs.length) {
				for (var i = 0; i < inputs.length; i++) {
					inputs[i].addEventListener('blur', orderAmountHandler);
				}
			}
		}
	}
	init();
	
})(this, this.document); // Pass in a reference to the original global window object.
