var WebSocketServer = require('websocket').server;
var http = require('http');
var mysql = require('mysql');

var mysqlConnection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '9101ch18',
  database : 'memory',
});
mysqlConnection.connect();

var server = http.createServer(function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});

server.listen(8080, function() {
    console.log((new Date()) + ' Server is listening on port 8080');
});

wsServer = new WebSocketServer({
  httpServer: server,
  autoAcceptConnections: false
});

function originIsAllowed(origin) {
  // put logic here to detect whether the specified origin is allowed.
  return true;
}

// clients connected with server.
GLOBAL.clients = [];
wsServer.on('request', function(request) {
    if (!originIsAllowed(request.origin)) {
        // Make sure we only accept requests from an allowed origin
        request.reject();
        return;
    }
    
    var connection = request.accept('echo-protocol', request.origin);
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            var request = JSON.parse(message.utf8Data);
            
            switch (request.method) {
                case 'put':
                        var post  = {
                            name: request.data.name, 
                            time: request.data.time
                        };
                        
                        mysqlConnection.query('INSERT INTO highscore SET ?', post, function(err, rows, fields) {
                            notifyAllClients();
                        });
                    break;
                case 'get':
                    mysqlConnection.query('SELECT * FROM highscore WHERE time >= 0 ORDER BY time ASC LIMIT 10', function(err, rows, fields) {
                        connection.send(JSON.stringify({
                            response: '200',
                            action: 'highscore',
                            data: rows
                        }));
                    });
                    break;
            }
        }
    });
    
    connection.on('close', function(message) {
        // TODO: CLEAN UP RESOURCES..
    });
    
    // add connection to client array.
    GLOBAL.clients.push(connection);
});

/**
 * Notify all clients that a new score has been added.
 */
function notifyAllClients() {
    mysqlConnection.query('SELECT * FROM highscore WHERE time >= 0 ORDER BY time ASC LIMIT 10', function(err, rows, fields) {
        var clients = GLOBAL.clients;
        for (i = 0; i < clients.length; i++) {
            clients[i].send(JSON.stringify({
                response: '200',
                action: 'highscore',
                data: rows
            }));
        }
    });
}