(function(winbdow, document, $) {
    GameStats = function(args) {
        /**
         * a reference to the memory game.
         */
        var self = this;
        
        /**
         * a WebSocket object.
         */
        var connection;
        
        var opts = $.extend(true, {
            url: 'ws://localhost:8080',
            protocols: ['echo-protocol'],
            onOpen: function() {},
            onClose: function() {},
            onMessage: function() {},
            onError: function() {}
        }, args);
    
        /**
         * Open a new socket connection. Calling this function
         * when a connection already exists is a no-op.
         *
         * @param {Object} an optional object containing options to operate on.
         * @return {WebSocket} a new or existing WebSocket object.
         */
        this.open = function(args) {
            // merge contents of the given argument into opts.
            if (Object.prototype.toString.call(args) === '[object Object]') {
                $.extend(true, opts, args);
            }

            if (!self.isOpen()) {
                connection = new WebSocket(opts.url, opts.protocols);
                connection.addEventListener('open', opts.onOpen);
                connection.addEventListener('close', opts.onClose);
                connection.addEventListener('message', opts.onMessage);
                connection.addEventListener('error', opts.onError);
            }
            return connection;
        }
        
        /**
         * Close an existing connection. Calling this function
         * when no connection is open is a no-op.
         */
        this.close = function() {
            if (self.isOpen()) {
                connection.removeEventListener('open', opts.onOpen);
                connection.removeEventListener('close', opts.onClose);
                connection.removeEventListener('message', opts.onMessage);
                connection.removeEventListener('error', opts.onerror);
                connection.close();
            }
        }
    
        /**
         * Returns true if connection is open, false otherwise.
         *
         * @return {Boolean} true if connection is open, false otherwise.
         */
        this.isOpen = function() { 
            return (self.getReadyState() == 1);
        }
        
        /**
         * Returns the state of the WebSocket connection. If no WebSocket connection exists
         * -1 will be returned.
         *
         * @return {Number} a constant describing the connection state.
         * @link https://developer.mozilla.org/en-US/docs/Web/API/WebSocket#Ready_state_constants
         */
        this.getReadyState = function() {
            return (connection instanceof WebSocket) ? connection.readyState : -1;
        }
        
        /**
         * Returns an Object containing the 10 highest scores.
         *
         * @return {Object} the ten highest scores.
         */
        this.getScores = function() {
            // program logic errors should lead to a code fix.
            if (!self.isOpen()) {
                throw new Error('connection is closed, call open() function first.');
            }
            
            // WebSocket connection needs to be open.
            if (self.getReadyState() == 1) {
                connection.send(JSON.stringify({
                    method: 'get'
                }));
            }
        }
        
        /**
         * Send score to server using an existing connection.
         *
         * @param {Object} args the values to send.
         */
        this.sendScore = function(args) {
            // program logic errors should lead to a code fix.
            if (!self.isOpen()) {
                throw new Error('connection is closed, call open() function first.');
            }
        
            var values = $.extend({
                name: '',
                time: -1,
            }, args);
            
            // WebSocket connection needs to be open.
            if (self.getReadyState() == 1) {
                connection.send(JSON.stringify({
                    method: 'put',
                    data: values
                }));
            }
        }
    } 
    
    // add GameStats object to the global scope.
	window.GameStats = GameStats;
    
})(this, this.document, jQuery);