(function($) {
    $.fn.memoryGame = function(args) {
        /**
         * a reference to the memory game.
         */
        var self = this;
        
        /**
         * the viewport which contains all elements of the memory game.
         */
        var $viewport;
        
        /**
         * the amount of cards to generate.
         */
        var deckSize;
        
        /**
         * array containing the visible cards for each turn.
         */
        var visibleCards = [];
        
        /**
         * array containing cards that have been matched.
         */
        var matchedCards = [];
        
        /**
         * interval ID for the time bar. 
         */
        var countdownId;
        
        /**
         * a Date object for calculating the turn time.
         */         
        var turnTime;
        
        /**
         * total time in milliseconds it took to complete the game.
         */
        var elapsedTime = 0;
        
        /**
         * options used to create the memory game.
         */
        var opts = $.extend(true, {
            timeLimit: 3,
            deckSize: 24,
            timer: true,
            timerSelector: null,
            timerDuration: 75,
            wrapperClass: 'memory-game-viewport',
            onFinished: function() {}
        }, args);

        /**
         * Utility object containing functions that operate on String object.
         */
        var StringUtilities = {
            /**
             * Repeat a String repeat times to form a new String.
             *
             * @param string str the String to repeat, may be null.
             * @param int times number of times to repeat the given string.
             * @return string a new String consisting of the original string repeated.
             */
            repeat: function(str, times) { 
                times = (typeof times === 'number') ? Math.abs(times) : 0;
                return (new Array(times + 1)).join(str);
            },
            /**
             * Shuffles the characters of the original String to form a new String.
             *
             * @param string str the string whose characters to shuffle
             * @return string a new String whose characters have been shuffled.
             */
            shuffle: function(str) {
                var parts = str.split('');
                for (var i = parts.length; i > 0;) {
                    var random = parseInt(Math.random() * i);
                    var temp = parts[--i];
                    parts[i] = parts[random];
                    parts[random] = temp;
                }
                return parts.join('');
            }
        };
        
        
        function appendCards() {
            // number of card to created.
            var deckSize = getDeckSize();        
            // the length of random character string.
            var stringSize = (deckSize >> 1);
            // double the size of random characters and shuffle position of characters.
            var letters = StringUtilities.shuffle(StringUtilities.repeat(randChars(stringSize), 2));

            var $board = $(self);
            for (i = 0; i < deckSize; i++) {
                $board.append(
                    $(document.createElement('div')).append(
                        $(document.createElement('div')).append(
                            $(document.createElement('div')).addClass('front').text('?')
                        ).append(
                            $(document.createElement('div')).addClass('back').text(letters.charAt(i))
                        ).addClass('wrapper')
                    ).addClass('card flippable').attr('data-letter', letters.charAt(i))
                );
            }
        }
        
        /**
         * Returns a String containing random characters.
         *
         * @param int len the number of random characters to generate.
         * @param string charSet a String containing one or more allowed characters.
         */
        function randChars(len, charSet) {
            // make sure we have a valid set of characters.
            charSet = (typeof charSet === 'string') ? charSet : 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            // we can only work with a positive number.
            len = (typeof len === 'number') ? Math.abs(len) : 0;
            
            var randomString = '';
            for (i = 0; i < len; i++) {
                var index = (Math.random() * charSet.length);
                randomString += charSet.charAt(index);
            }
            return randomString;
        }
        
        /**
         * Returns the number of card to create.
         *
         * @return {Number} the number of cards to create.
         */
        function getDeckSize() {
            if (typeof deckSize == 'undefined') {
                deckSize = (typeof opts.deckSize === 'number') ? parseInt(Math.abs(opts.deckSize), 10) : 24;
            }
            return deckSize; 
        }
        
        /**
         * Returns the class name for the wrapper element.
         *
         * @return {String} a class name for the wrapper element.
         */
        function getWrapperClass() {
            var className = 'memory-game-wrapper';
            if (typeof opts.wrapperClass === 'string') {
                className = opts.wrapperClass;
            }
            return className;
        }
        
        /**
         * Appends a timer to the board element.
         */
        function appendTimer() {
            // create a timer element.
            var $timebar = $(document.createElement('div')).append(
                $(document.createElement('span')).addClass('bar')
            ).addClass('timer');
        
            if (typeof opts.timerSelector === 'string') {
                // append timer to an existing element.
                $(opts.timerSelector).append($timebar);
            } else {
                // append timer to a new element.
                $(self).after(
                    $(document.createElement('div')).append(
                        $timebar
                    ).addClass('timer-wrapper')
                );
            }
        }
        
        /**
         * Returns true if the card at the specified index can rotate, false otherwise.
         *
         * @param {int} cardIndex the index of the card to test.
         * @return {bool} true if the card can be rotated, false otherwise.
         */
        function canRotate($card) {
            if ($card instanceof jQuery) {
                // assume this card is rotable.
                var rotable = true;
                // unable to rotate already visible card.
                if ($.inArray($card.get(0), visibleCards) > -1) {
                    rotable = false;
                }
                // unable to rotate cards that have matched.
                if ($.inArray($card.get(0), matchedCards) > -1) {
                    rotable = false;
                }
                
                return rotable;
            }   
            
            // only jQuery elements are rotable.
            return false;
        }
        
        function rotateCard(cardIndex) {           
            var $flippedCard = $(self).children().eq(parseInt(cardIndex));
            if (canRotate($flippedCard)) {
                // start countdown when card rotates.
                if (!isRunning()) {
                    startCountdown();
                }
            
                // flip this card.
                $flippedCard.addClass('flipped');
                // push card to the end of the array.
                visibleCards.push($flippedCard.get(0));
    
                // a card that matched with the flipped card.
                var $matchedCard = null;
                // compare all visible cards and determine if they match.
                $.each(visibleCards, function(index, card) {
                    var $currentCard = $(card);
                    // determine if letter of cards match. 
                    if (!$currentCard.is($flippedCard) && $currentCard.attr('data-letter') == $flippedCard.attr('data-letter')) {
                        // store reference to matched card.
                        $matchedCard = $currentCard;
                        // break jQuery loop.
                        return false;
                    }
                });

                if ($matchedCard) {
                    // push both cards to the end of the array/
                    matchedCards.push($matchedCard.get(0), $flippedCard.get(0));
                    
                    $flippedCard.addClass('matched');
                    $matchedCard.addClass('matched');
                    
                    // a matching pair was found.
                    endTurn();
                }
            }
        }
        
        /**
         * Click handler that is called whenever a card is cicked.
         *
         * @param object e object containing information about the event.
         */
        function clickHandler(e) {        
            // flip two cards for each turn.
            if (visibleCards.length < 2) {
                rotateCard($(this).index()); 
            }
        }
        
        /**
         * Starts the timebar from counting down.
         */
        function startCountdown() {
            var $progressBar = $viewport.find('.timer .bar').eq(0);
            var startTime = getDateTime(true).getTime();
            var maxTurnTime = self.getMaxTurnTime();

            countdownId = setInterval(function() {
                // remaining time for this turn.
                var remainingTime = maxTurnTime - (new Date().getTime() - startTime);
                if (remainingTime > 0) {
                    if ($progressBar.length) {
                        // calculate width of progress bar.
                        var percentage = (remainingTime > 0) ? (100 / (maxTurnTime / remainingTime)) : 0;
                        // update width of progressBar.
                        $progressBar.css({
                            display: 'block',
                            width:  percentage + '%'
                        });
                    }
                } else {
                    // time for this turn has elapsed.
                    endTurn();
                }
            }, opts.timerDuration);
        }
        
        /**
         * Update elapsed time with the time each turn took.
         */
        function updateElapsedTime() {
            elapsedTime += (new Date().getTime() - getDateTime().getTime());
        }
        
        /**
         * Stops the time bar from counting down.
         */
        function stopCountdown() {
            if (countdownId) {
                clearInterval(countdownId);
            }
            countdownId = null;
        }
        
        /**
         * Stop time bar and reset progress bar to it's initial state.
         */
        function resetCountdown() {
            // stop count down.
            stopCountdown();
                   
            // reset progress bar.
            var $progressBar = $viewport.find('.timer .bar').eq(0);
            if ($progressBar.length) {
                $progressBar.width('100%');
            }
        }
        
        function endTurn() {        
            // remove classes from card that didn't match.
            $.each(visibleCards, function(index, card) {
                if (!$(card).hasClass('matched')) {
                    $(card).removeClass('flipped');
                }
            });
            // empty array with visible cards.
            visibleCards = [];
            
            // store elapsed time for this turn.
            updateElapsedTime();
            // reset the progress bar.
            resetCountdown();
            
            // call user on finished handler.
            if (isGameCompleted() && (typeof opts.onFinished === 'function')) {
                opts.onFinished.call(self, elapsedTime);
            }
        }
        
        /**
         * Returns true the timebar is running, false otherwise.
         *
         * @return {Boolean} true if the timebar is running, false otherwise.
         */
        function isRunning() {
            return (!(typeof countdownId === 'undefined' || countdownId === null));
        }
        
        /**
         * Returns true if all cards have been matched, false otherwise.
         *
         * @return {Boolean} true if all cards have been match, false otherwise.
         */
        function isGameCompleted() {
            return (matchedCards.length == getDeckSize());
        }
        
        /**
         * Returns a Date object to work with dates and times..
         *
         * @param {boolean} reset if true a new Date object is created, otherwise an existing
         *                  Date object may be reused.
         * @return a Date object.
         */
        function getDateTime(reset) {
            if (!(turnTime instanceof Date) || ((typeof reset === 'boolean') && reset)) {
                turnTime = new Date();
            }
            return turnTime;
        }
        
        /**
         * a constructor to setup the memory game.
         */
        function init() {       
            var $parent = $(self).parent();
            var wrapperClass = getWrapperClass();
            if (!($parent.is('div') && $parent.hasClass(wrapperClass))) {
                var $wrapper = $(document.createElement('div')).addClass(wrapperClass);
                $(self).wrap($wrapper);
            }
            // store reference to viewport.
            $viewport = $(self).parent(); 
            
            // append cards and timer to the game.
            appendCards();
            if (typeof opts.timer === 'boolean' && opts.timer) {
                appendTimer();
            }
            
            // enable flipping of cards.
            $(self).on('click', '.card', clickHandler);
        }
        
        // call the constructor.
        init();

        /**
         * Set the maximum turn time in seconds.
         *
         * @param {Number} the maximum turn time.
         */
        this.setMaxTurnTime = function(time) {
            if ($.isNumeric(time)) {
                opts.timeLimit = parseInt(time);
            }
        }
        
        /**
         * Returns the maximum turn time in milliseconds.
         *
         * @return {Number} the maximum turn time.
         */
        this.getMaxTurnTime = function() {
            return (opts.timeLimit * 1000);
        }
        
        /**
         * Returns the elapsed time in milliseconds.
         *
         * @return {Number} the elapsed time.
         */
        this.getElapsedTime= function() {
            return elapsedTime;
        }

        // returns the current jQuery object
        return this;
    }
})(jQuery);
