(function(window, document, undefined) {

	// only add function to prototype if it doesn't exist.
	if (typeof String.prototype.format !== 'function') {
		/**
		 * Returns a formatted string with the given number of arguments.
		 *
		 * @param mixed arguments a variable number of arguments.
		 * @return string a formatted string.
		 */
		String.prototype.format = function() {
			var args = arguments;
			return this.replace(/{(\d+)}/g, function(match, number) { 
				return (typeof args[number] != 'undefined') ? args[number] : match;
			});
		};
	}
        
})(this, this.document); // Pass in a reference to the original global window object.
